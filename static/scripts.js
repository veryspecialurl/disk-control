$(document).ready(function() {
	var selected_folder = ''
	$(document).click(function(event) {
		if ($(event.target).is('#trash_clean_link')) {
			trash_clear();
		}
		if ($(event.target).is('#upload-file-link')) {
			$('input#upload_path').attr({
				value: $('#path_line').text()
			});
			$('.upload_form_div').fadeIn('fast', function() {});
		}
		if ($(event.target).is('#google-upload-file-link')) {
			$('.google_upload_form_div').fadeIn('fast', function() {});
		}
		close_all_context_menu();
		if ($(event.target).is('.block-close')) {} else {
			$('.main_form').fadeOut('fast', function() {});
		}
		if (!$(event.target).is('.yandex_menu_link') && !$(event.target).is('.block-close')) {
			close_popup_window();
		}
		if ($(event.target).is('.folder-select')) {
			if (selected_folder != '') {
				$(`span[data-identificator="${selected_folder}"]`).css({
					'background-color':'#1B1B1B',
				})
			}
			selected_folder = $(event.target).attr('data-identificator');
			$(`span[data-identificator="${selected_folder}"]`).css({
				'background-color':'#FF5001',
			})
		}
		if ($(event.target).is('.choose_folder_button')) {
			if (selected_folder == '') {
				return;
			}
			choosed_folder = `span[data-identificator="${selected_folder}"]`
			var data = {
					name: $(choosed_folder).attr('data-title'),
					path_from: $(choosed_folder).attr('data-from_path')
				}
			if ($(choosed_folder).attr('data-identificator') != '*') {
				data['path_to'] = $(choosed_folder).attr('data-identificator')
			}
			$.get('/yandexmovefile/', data, function(response) {
				if (response.success == true) {
					var id = $(`span[name="${$(choosed_folder).attr('data-from_path')}"]`).attr('id');
					$(`#del${id}`).remove();
					close_popup_window();
				} else {
					close_popup_window();
					create_popup_window();
					$('<span>', {
						text: response.message
					}).appendTo('#popup_window_col_body');
					show_popup_window();
				}
			});
		}
		if ($(event.target).is('.google_choose_folder_button')) {
			if (selected_folder == '') {
				return;
			}
			choosed_folder = `span[data-identificator="${selected_folder}"]`
			var data = {
					path_from: $(choosed_folder).attr('data-from_path')
				}
			if ($(choosed_folder).attr('data-identificator') != '*') {
				data['path_to'] = $(choosed_folder).attr('data-identificator')
			}
			$.get('/googlemovefile/', data, function(response) {
				if (response.success == true) {
					$(`#del${$(choosed_folder).attr('data-from_path')}`).remove();
					close_popup_window();
				} else {
					close_popup_window();
					create_popup_window();
					$('<span>', {
						text: response.message
					}).appendTo('#popup_window_col_body');
					show_popup_window();
				}
			});
		}
	});
	$(".disk-main").on("contextmenu", false);
	$('.item_link').contextmenu(function(event) {
		var path = $(this).attr('name');
		var id = '#' + $(this).attr('id');
		var offset = $(this).offset();
		close_all_context_menu();
		close_popup_window();
		$('<div>', {
			class: 'yandex_menu'
		}).appendTo(id);
		if ($(this).attr('data-trash') === 'True') {
			$('<a>', {
				class: 'yandex_menu_link',
				text: 'Восстановить',
				name: path,
				id: id,
				onclick: 'trash_restore_file(this)'
			}).appendTo('.yandex_menu');
			$('<a>', {
				class: 'yandex_menu_link delete_trash_item_link',
				text: 'Удалить',
				onclick: 'trash_clear(this)',
				name: path,
				id: $(this).attr('id')
			}).appendTo('.yandex_menu');
		} else {
			$('<a>', {
				class: 'yandex_menu_link rename_item_link block-close',
				name: path,
				onclick: 'rename_file(this)',
				text: 'Переименовать'
			}).appendTo('.yandex_menu');
			if ($(this).attr('data-public') === 'True') {
				$('<a>', {
					class: 'yandex_menu_link show_public_url',
					text: 'Показать ссылку на файл',
					id: id,
					onclick: 'show_public_url(this)'
				}).appendTo('.yandex_menu');
				$('<a>', {
					class: 'yandex_menu_link delete_public_url',
					text: 'Удалить ссылку на файл',
					id: id,
					name: path,
					onclick: 'yandex_unpublish(this)',
					'data-type': $(this).attr('data-type')
				}).appendTo('.yandex_menu');
			} else {
				$('<a>', {
					class: 'yandex_menu_link',
					text: 'Переместить',
					id: $(this).attr('id'),
					'data-title': $(this).attr('title'),
					'data-path': path,
					onclick: 'yandex_move_folders(this)',
					'data-type': $(this).attr('data-type')
				}).appendTo('.yandex_menu');
				$('<a>', {
					class: 'yandex_menu_link',
					text: 'Поделиться',
					id: id,
					name: path,
					onclick: 'yandex_publish(this)',
					'data-type': $(this).attr('data-type')
				}).appendTo('.yandex_menu');
				$('<a>', {
					class: 'yandex_menu_link delete_item_link block-close',
					text: 'Удалить',
					onclick: 'show_choose_delete_method(this)',
					name: path,
					id: $(this).attr('id')
				}).appendTo('.yandex_menu');
			}
			$('<a>', {
				class: 'yandex_menu_link download_item_link',
				name: path,
				onclick: 'get_download_link(this)',
				text: 'Скачать'
			}).appendTo('.yandex_menu');
		}
		$('.yandex_menu').css({
			display: "none",
			position: 'absolute',
			top: event.pageY - offset.top,
			left: event.pageX - offset.left
		});
		$('.yandex_menu').fadeIn('fast', function() {});
	});
	$('.item_link_google').contextmenu(function(event) {

		close_all_context_menu();
		close_popup_window();

		var offset = $(this).offset();
		var id = $(this).attr('id');
		var data_trash = $(this).attr('data-trash');
		var data_public = $(this).attr('data-public');

		$('<div>', {
			class: 'google_menu'
		}).appendTo('#' + id);
		$('<a>', {
			class: 'yandex_menu_link delete_item_google',
			text: 'Удалить',
			'data-trash': data_trash,
			onclick: 'google_show_choose_delete_method(this)',
			'data-id': id
		}).appendTo('.google_menu');
		$('<a>', {
			class: 'yandex_menu_link rename_item_google block-close',
			text: 'Переименовать',
			onclick: 'google_rename_file(this)',
			'data-id': id
		}).appendTo('.google_menu');
		if (data_public === "True") {
			$('<a>', {
				class: 'yandex_menu_link download_item_google',
				'data-id': id,
				onclick: 'google_get_download_link(this)',
				text: 'Показать ссылку на файл'
			}).appendTo('.google_menu');
			$('<a>', {
				class: 'yandex_menu_link download_item_google',
				'data-id': id,
				onclick: 'google_publish(this)',
				text: 'Удалить ссылку на файл'
			}).appendTo('.google_menu');
		} else {
			$('<a>', {
				class: 'yandex_menu_link download_item_google',
				'data-id': id,
				onclick: 'google_get_download_link(this)',
				text: 'Скачать'
			}).appendTo('.google_menu');
			$('<a>', {
				class: 'yandex_menu_link download_item_google',
				'data-id': id,
				onclick: 'google_publish(this)',
				text: 'Опубликовать'
			}).appendTo('.google_menu');
			$('<a>', {
				class: 'yandex_menu_link',
				text: 'Переместить',
				id:id,
				onclick:'google_move_folders(this)'
			}).appendTo('.google_menu');
		}
		$('.google_menu').css({
			display: "none",
			position: 'absolute',
			top: event.pageY - offset.top,
			left: event.pageX - offset.left
		});
		$('.google_menu').fadeIn('fast', function() {});
	});
	$('#make_new_dir').dblclick(function(event) {
		$('.form_div').fadeIn('fast', function() {});
	});
	$('#make_new_dir_google').dblclick(function(event) {
		$('.new_google_folder').fadeIn('fast', function() {});
	});
	$('.item_link, .item_link_google').dblclick(function(event) {
		if ($(this).attr('data-trash') === 'True') {
			create_popup_window();
			$('<span>', {
				text: 'В данной версии, в корзине не является возможным открыть папку или файл'
			}).appendTo('#popup_window_col_body');
			show_popup_window();
			return;
		}
		if ($(this).attr('data-type') == 'dir') {
			$(location).attr('href', '/' + $(this).attr('name'));
		}
		if ($(this).attr('data-type') == 'application/vnd.google-apps.folder') {
			$(location).attr('href', '/g/' + $(this).attr('id'));
		}
	});
});

function close_all_context_menu() {
	$('.yandex_menu').fadeOut('fast', function() {});
	$('.yandex_menu').remove();
	$('.google_menu').fadeOut('fast', function() {});
	$('.google_menu').remove();
}

function create_popup_window() {
	close_popup_window();
	$('<div>', {
		class: 'popup_window block-close'
	}).appendTo('.main');
	$('<div>', {
		class: 'container-fluid block-close',
		id: 'popup_window_container'
	}).appendTo('.popup_window')
	$('<div>', {
		class: 'row block-close',
		id: 'popup_window_row'
	}).appendTo('#popup_window_container')
	$('<div>', {
		class: 'col-12 p-0 block-close',
		id: 'popup_window_col_body'
	}).appendTo('#popup_window_row')
}

function close_popup_window() {
	if ($('.popup_window').length) {
		$('.popup_window').remove()
	}
}

function show_popup_window() {
	$('.popup_window').css({
		left: ($(window).width() - $('.popup_window').width()) / 2,
		top: $('html').offset().top + ($(window).height() - $('.popup_window').height()) / 2
	})
	$('.popup_window').fadeIn('fast', function() {});
}

function get_download_link(elem) {
	$.getJSON('/ydownload/' + $(elem).attr('name'), function(data) {
		jsoni = JSON.parse(data);
		create_popup_window();
		$('<a>', {
			download: 'download',
			id: 'download_file_link',
			href: jsoni.href,
			text: 'Скачать'
		}).appendTo('#popup_window_col_body');
		show_popup_window();
	});
}

function show_choose_delete_method(elem) {
	var path = $(elem).attr('name')
	var js_path = $(elem).attr('id')
	create_popup_window();
	$('<span>', {
		text: 'Выберите способ удаления файла:'
	}).appendTo('#popup_window_col_body');
	$('<button>', {
		text: 'Удалить навсегда',
		'data-path': path,
		'data-jspath': js_path,
		'data-permanent': 'true',
		onclick: 'delete_method(this)'
	}).appendTo('#popup_window_col_body');
	$('<button>', {
		text: 'Поместить в корзину',
		'data-path': path,
		'data-jspath': js_path,
		'data-permanent': 'false',
		onclick: 'delete_method(this)'
	}).appendTo('#popup_window_col_body');
	show_popup_window();
}

function delete_method(elem) {
	var permanent = $(elem).attr('data-permanent');
	var path = $(elem).attr('data-path');
	var js_path = $(elem).attr('data-jspath');
	if (permanent === 'true') {
		url_text = '/ydelete/' + path + '+true';
	} else {
		url_text = '/ydelete/' + path + '+false';
	}
	$.ajax({
			url: url_text,
			type: 'GET',
		})
		.done(function() {
			$('#del' + js_path).remove();
			close_popup_window();
		})
}

function trash_restore_file(elem) {
	$.ajax({
			url: '/yrestore' + $(elem).attr('name'),
			type: 'GET',
		})
		.always(function() {
			$($(elem).attr('id')).remove();
			close_popup_window();
		});
}

function rename_file(elem) {
	$('#preveous_name').attr({
		value: $(elem).attr('name')
	});
	$('.rename_form_div').fadeIn('fast', function() {});
}

function yandex_publish(elem) {
	$.getJSON('/ypublish/' + $(elem).attr('name'), function(data) {
		jsoni = JSON.parse(data);
		create_popup_window();
		$('<span>', {
			text: 'Публичная ссылка на файл:'
		}).appendTo('#popup_window_col_body');
		$('<a>', {
			href: jsoni.public_url,
			text: jsoni.public_url
		}).appendTo('#popup_window_col_body');
		show_popup_window();
		if ($(elem).attr('data-type') == 'dir') {
			$($('img' + $(elem).attr('id'))).attr('src', '/static/dir_public.png');
		} else {
			$($('img' + $(elem).attr('id'))).attr('src', '/static/file_public.png');
		}
		$($(elem).attr('id')).attr('data-public', 'True');
		$($(elem).attr('id')).attr('data-public_url', jsoni.public_url);
	});
}

function yandex_unpublish(elem) {
	$.ajax({
			url: '/yunpublish/' + $(elem).attr('name'),
			type: 'GET',
		})
		.done(function(data) {
			if ($(elem).attr('data-type') == 'dir') {
				$($('img' + $(elem).attr('id'))).attr('src', '/static/dir.png');
			} else {
				$($('img' + $(elem).attr('id'))).attr('src', '/static/file.png');
			}
			$($(elem).attr('id')).attr('data-public', 'False');
			$($(elem).attr('id')).attr('data-public_url', '');
		});
}

function show_public_url(elem) {
	url = $($(elem).attr('id')).attr('data-public_url');
	create_popup_window();
	$('<span>', {
		text: 'Публичная ссылка на файл:'
	}).appendTo('#popup_window_col_body');
	$('<a>', {
		href: url,
		text: url
	}).appendTo('#popup_window_col_body');
	show_popup_window();
}

function trash_clear(elem) {
	$.ajax({
			url: '/ytrash_clear' + $(elem).attr('name'),
			type: 'GET'
		})
		.always(function() {
			$('#' + $(elem).attr('id')).remove();
		});
}

function google_show_choose_delete_method(elem) {
	var id = $(elem).attr('data-id')
	var data_trash = $(elem).attr('data-trash')
	create_popup_window();
	if (data_trash === 'False') {
		$('<span>', {
			text: 'Выберите способ удаления файла:'
		}).appendTo('#popup_window_col_body');
		$('<button>', {
			text: 'Удалить навсегда',
			'data-id': id,
			'data-permanent': 'true',
			onclick: 'google_delete_method(this)'
		}).appendTo('#popup_window_col_body');
		$('<button>', {
			text: 'Поместить в корзину',
			'data-id': id,
			'data-permanent': 'false',
			onclick: 'google_delete_method(this)'
		}).appendTo('#popup_window_col_body');
		show_popup_window();
	} else {
		$('<span>', {
			text: 'Выберите способ удаления файла<:></:>'
		}).appendTo('#popup_window_col_body');
		$('<button>', {
			text: 'Удалить навсегда',
			'data-id': id,
			'data-permanent': 'true',
			onclick: 'google_delete_method(this)'
		}).appendTo('#popup_window_col_body');
		show_popup_window();
	}
}

function google_delete_method(elem) {
	var permanent = $(elem).attr('data-permanent');
	var id = $(elem).attr('data-id');
	if (permanent === 'true') {
		url_text = '/gdelete/' + id + '+true';
	} else {
		url_text = '/gdelete/' + id + '+false';
	}
	$.ajax({
			url: url_text,
			type: 'GET',
		})
		.done(function() {
			$('#del' + id).remove();
			close_popup_window();
		})
}

function google_rename_file(elem) {
	$('#google_id').attr({
		value: $(elem).attr('data-id')
	});
	$('.google_rename_form_div').fadeIn('fast', function() {});
}

function google_get_download_link(elem) {
	$.getJSON('/ggetlink/' + $(elem).attr('data-id'), function(data) {
		create_popup_window();
		$('<a>', {
			href: data.href,
			text: data.href
		}).appendTo('#popup_window_col_body');
		show_popup_window();
	});
}

function google_publish(elem) {
	create_popup_window();
	$('<span>', {
		text: 'Операции связанные с публикацией файлов в данной версии не реализованы!'
	}).appendTo('#popup_window_col_body');
	show_popup_window();
	return None;
	$.ajax({
			url: '/gpublish/' + $(elem).attr('data-id'),
			type: 'GET',
		})
		.done(function() {
			if ($(elem).attr('data-type') == 'application/vnd.google-apps.folder') {
				$($('img#' + $(elem).attr('id'))).attr('src', '/static/dir.png');
			} else {
				$($('img#' + $(elem).attr('id'))).attr('src', '/static/file.png');
			}
			$($(elem).attr('id')).attr('data-public', 'False');
		})
}

function yandex_move_folders(elem) {
	create_popup_window();
	$('<div>', {
		class: 'row m-0 main_move_block'
	}).appendTo('#popup_window_col_body');
	$('<div>', {
		class: 'col-12',
		id: 'root-col-12'
	}).appendTo('.main_move_block');
	$('<div>', {
		class: 'row m-0 row_move_block',
		id: 'row_move_block'
	}).appendTo('#root-col-12*');
	$('<div>', {
		class: 'row m-0 row_pr_block',
		id: 'row_pr_block'
	}).appendTo('#root-col-12');
	$('<div>', {
		class: 'p-0 col-1 block-close',
		id: 'root-col-1'
	}).appendTo('#row_move_block');
	$('<div>', {
		class: 'col-11 block-close',
		id: 'root-col-11'
	}).appendTo('#row_move_block');
	$('<img>', {
		class: 'move_folder_img img-fluid block-close',
		src: '../static/arrow_down.png',
		onclick: 'yandex_load_folders(this)',
		'data-first': 'False',
		'data-opened': 'False',
		'data-title': $(elem).attr('data-title'),
		'data-id':'/',
		'data-path':'*',
		'data-from_path':$(elem).attr('data-path'),
		 id:''
	}).appendTo($('.row_move_block').children('#root-col-1'));
	$('<span>', {
		class: 'folder-select block-close',
		'data-identificator':'*',
		'data-title': $(elem).attr('data-title'),
		'data-from_path':$(elem).attr('data-path'),
	}).appendTo($('.row_move_block').children('#root-col-11'));
	$('#root-col-11').children('span').text('Корневой каталог');
	yandex_load_folders($('img[data-id="/"]'));
	$('<a>', {
		href:'#',
		class: 'block-close choose_folder_button',
		text:'Переместить'
	}).appendTo('#popup_window_col_body');
	show_popup_window();
}

function yandex_load_folders(elem) {
	var url = ''
	if ($(elem).attr('data-path') == '*') {
		url = '/yloadfolderss/';
	} else {
		url = '/yloadfolders/' + $(elem).attr('data-path');
	}
	if ($(elem).attr('data-first') == 'False') {
		$.getJSON(url, function(data) {
				$(elem).attr('data-first', 'True');
				if (data.items.length == 0) {
					if ($(elem).attr('data-opened') == 'False') {
						$(elem).css({transform: 'rotate(180deg)', transition: '0.5s'})
						$(elem).attr('data-opened', 'True');
					} else {
						$(elem).css({transform: 'rotate(0deg)', transition: '0.5s'})
						$(elem).attr('data-opened', 'False');
					}
					return;
				}
				$.each(data.items, function(index, value) {
					if (value['name'] != $(elem).attr('data-title')) {
						var js_path = value['js_path']
						$('<div>', {
						class: 'col-12',
						id: 'col-12' + js_path
						}).appendTo('#row_pr_block' + $(elem).attr('id'));
						$('<div>', {
							class: 'row m-0 row_move_block',
							id: 'row_move_block' + js_path
						}).appendTo('#col-12' + js_path);
						$('<div>', {
							class: 'row m-0 row_pr_block',
							id: 'row_pr_block' + js_path
						}).appendTo('#col-12' + js_path);
						$('<div>', {
							class: 'col-1 block-close p-0',
							id: 'col-1' + js_path
						}).appendTo('#row_move_block' + js_path);
						$('<div>', {
							class: 'col-11 block-close',
							id: 'col-11' + js_path
						}).appendTo('#row_move_block' + js_path);
						$('<img>', {
							class: 'move_folder_img img-fluid block-close',
							id: js_path,
							src: '../static/arrow_down.png',
							onclick: 'yandex_load_folders(this)',
							'data-path': value['path'],
							'data-first': 'False',
							'data-opened': 'False',
							'data-title': $(elem).attr('data-title'),
							'data-from_path':$(elem).attr('data-from_path')
					}).appendTo($('.row_move_block').children('#col-1' + js_path));
						$('<span>', {
							class: 'block-close folder-select',
							'data-identificator':value['path'],
							'data-title': $(elem).attr('data-title'),
							'data-from_path':$(elem).attr('data-from_path')
						}).appendTo($('.row_move_block').children('#col-11' + js_path));
						$('#col-11' + js_path).children('span').text(value['name']);
					}
				});
			});
		}
		if ($(elem).attr('data-opened') == 'False') {
			$('#row_pr_block' + $(elem).attr('id')).css({display:"flex"})
			$(elem).css({transform: 'rotate(180deg)', transition: '0.5s'})
			$(elem).attr('data-opened', 'True');
		} else {
			$('#row_pr_block' + $(elem).attr('id')).css({display:'None'})
			$(elem).css({transform: 'rotate(0deg)', transition: '0.5s'})
			$(elem).attr('data-opened', 'False');
		}
	}

	function google_move_folders(elem) {
	create_popup_window();
	$('<div>', {
		class: 'row m-0 main_move_block'
	}).appendTo('#popup_window_col_body');
	$('<div>', {
		class: 'col-12',
		id: 'root-col-12'
	}).appendTo('.main_move_block');
	$('<div>', {
		class: 'row m-0 row_move_block',
		id: 'row_move_block'
	}).appendTo('#root-col-12*');
	$('<div>', {
		class: 'row m-0 row_pr_block',
		id: 'row_pr_block'
	}).appendTo('#root-col-12');
	$('<div>', {
		class: 'p-0 col-1 block-close',
		id: 'root-col-1'
	}).appendTo('#row_move_block');
	$('<div>', {
		class: 'col-11 block-close',
		id: 'root-col-11'
	}).appendTo('#row_move_block');
	$('<img>', {
		class: 'move_folder_img img-fluid block-close',
		src: '../static/arrow_down.png',
		onclick: 'google_load_folders(this)',
		'data-first': 'False',
		'data-opened': 'False',
		'data-id':'/',
		'data-path':'*',
		'data-from_path':$(elem).attr('id'),
		'data-title':$(elem).attr('title'),
		id:''
	}).appendTo($('.row_move_block').children('#root-col-1'));
	$('<span>', {
		class: 'folder-select block-close',
		'data-identificator':'*',
		'data-from_path':$(elem).attr('id'),
	}).appendTo($('.row_move_block').children('#root-col-11'));
	$('#root-col-11').children('span').text('Корневой каталог');
	google_load_folders($('img[data-id="/"]'));
	$('<a>', {
		href:'#',
		class: 'block-close google_choose_folder_button',
		text:'Переместить'
	}).appendTo('#popup_window_col_body');
	show_popup_window();
}

function google_load_folders(elem) {
	var url = ''
	if ($(elem).attr('data-path') == '*') {	
		url = '/gloadfolderss/';
	} else {
		url = '/gloadfolders/' + $(elem).attr('data-path');
	}
	if ($(elem).attr('data-first') == 'False') {
		$.getJSON(url, function(data) {
				$(elem).attr('data-first', 'True');
				if (data.items.length == 0) {
					if ($(elem).attr('data-opened') == 'False') {
						$(elem).css({transform: 'rotate(180deg)', transition: '0.5s'})
						$(elem).attr('data-opened', 'True');
					} else {
						$(elem).css({transform: 'rotate(0deg)', transition: '0.5s'})
						$(elem).attr('data-opened', 'False');
					}
					return;
				}
				$.each(data.items, function(index, value) {
					if (value['name'] != $(elem).attr('data-title')) {
						var js_path = value['id']
						console.log(js_path)
						$('<div>', {
						class: 'col-12',
						id: 'col-12' + js_path
						}).appendTo('#row_pr_block' + $(elem).attr('id'));
						$('<div>', {
							class: 'row m-0 row_move_block',
							id: 'row_move_block' + js_path
						}).appendTo('#col-12' + js_path);
						$('<div>', {
							class: 'row m-0 row_pr_block',
							id: 'row_pr_block' + js_path
						}).appendTo('#col-12' + js_path);
						$('<div>', {
							class: 'col-1 block-close p-0',
							id: 'col-1' + js_path
						}).appendTo('#row_move_block' + js_path);
						$('<div>', {
							class: 'col-11 block-close',
							id: 'col-11' + js_path
						}).appendTo('#row_move_block' + js_path);
						$('<img>', {
							class: 'move_folder_img img-fluid block-close',
							id: js_path,
							src: '../static/arrow_down.png',
							onclick: 'google_load_folders(this)',
							'data-path': value['id'],
							'data-first': 'False',
							'data-opened': 'False',
							'data-title': $(elem).attr('data-title'),
							'data-from_path':$(elem).attr('data-from_path')
					}).appendTo($('.row_move_block').children('#col-1' + js_path));
						$('<span>', {
							class: 'block-close folder-select',
							'data-identificator':value['id'],
							'data-title': $(elem).attr('data-title'),
							'data-from_path':$(elem).attr('data-from_path')
						}).appendTo($('.row_move_block').children('#col-11' + js_path));
						$('#col-11' + js_path).children('span').text(value['name']);
					}
				});
			});
		}
		if ($(elem).attr('data-opened') == 'False') {
			$('#row_pr_block' + $(elem).attr('id')).css({display:"flex"})
			$(elem).css({transform: 'rotate(180deg)', transition: '0.5s'})
			$(elem).attr('data-opened', 'True');
		} else {
			$('#row_pr_block' + $(elem).attr('id')).css({display:'None'})
			$(elem).css({transform: 'rotate(0deg)', transition: '0.5s'})
			$(elem).attr('data-opened', 'False');
		}
	}