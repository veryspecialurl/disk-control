import mimetypes
import os

def filename(file):
    return file.name[file.name.rfind('\\')+1:]

def mimetype(file):
    return mimetypes.guess_type(filename(file))


with open('C:\dev\\flask\\disk-control\\requirements.txt', 'r') as file:
    print(filename(file))
    print(mimetype(file))