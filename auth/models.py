from manage import db
from flask_login import login_user
import auth.backends as Drive


class Users(db.Model):
    __tablename__ = 'Users'
    id = db.Column(db.Integer, primary_key=True)
    vk_id = db.Column(db.Integer, unique=True)
    vk_name = db.Column(db.String(100))
    vk_lastname = db.Column(db.String(100))
    google_drive = db.Column(db.PickleType, unique=True)
    yandex_drive = db.Column(db.PickleType, unique=True)
    google_drive_email = db.Column(db.Text, unique=True)
    yandex_drive_email = db.Column(db.Text, unique=True)
    is_active = db.Column(db.Boolean, default=True)

    def save(self):
        db.session.add(self)
        db.session.commit()

    def login(self):
        login_user(self, duration=300)
        Drive.Yandex.get_data()
        Drive.Google().get_data()

    def get_id(self):
        return self.id

    def is_authenticated(self):
        return True

    def is_anonymous(self):
        return False


class YandexDisk(db.Model):
    __tablename__ = 'Yandexdisk'
    id = db.Column(db.Integer, primary_key=True)
    total_space = db.Column(db.Integer)
    used_space = db.Column(db.Integer)
    folders = db.Column(db.PickleType)
