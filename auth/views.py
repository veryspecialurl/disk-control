from flask import Blueprint, redirect, url_for, session, request, jsonify
from flask_login import logout_user, current_user
import auth.backends as Backend
import logging

logging.basicConfig(level=logging.INFO)

auth = Blueprint('auth', __name__, template_folder='template', url_prefix='/auth')


@auth.route('/login_request')
def vk_login_request():
    redirect_uri = request.url_root + url_for('auth.vk_login_response')[1:]
    return redirect(Backend.Vk.make_request(redirect_uri))


@auth.route('/login_response')
def vk_login_response():
    redirect_uri = request.url_root + url_for('auth.vk_login_response')[1:]
    Backend.Vk.login(redirect_uri)
    return redirect(url_for('main.main_page'))


@auth.route('/ya_login_request')
def yandex_login_request():
    redirect_uri = request.url_root + url_for('auth.yandex_login_response')[1:]
    return redirect(Backend.Yandex.make_request(redirect_uri))


@auth.route('/ya_login_response')
def yandex_login_response():
    Backend.Yandex.login(code=request.args.get('code'))
    return redirect(url_for('main.main_page'))


@auth.route('/google_login_request')
def google_login_request():
    return redirect(Backend.Google.new_make_request())


@auth.route('/google_login_response')
def google_login_response():
    Backend.Google.new_login()
    return redirect(url_for('main.main_page'))


@auth.route('/yandex_logout')
def yandex_logout():
    Backend.Yandex().drive_logout()
    return redirect(url_for('main.main_page'))


@auth.route('/google_logout')
def google_logout():
    Backend.Google().drive_logout()
    return redirect(url_for('main.main_page'))


@auth.route('/logout')
def full_logout():
    Backend.Yandex().drive_logout()
    Backend.Google().drive_logout()
    if current_user.is_authenticated:
        logout_user()
    print(session)
    return redirect(url_for('main.main_page'))
