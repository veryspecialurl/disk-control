from flask import session, request, url_for, redirect, flash
from flask_login import current_user
from google.auth import jwt
from auth.models import Users
import settings
import requests
from manage import app
import google_auth_oauthlib.flow
from googleapiclient.discovery import build, MediaFileUpload
import google.auth.transport.requests
import google.oauth2.credentials
import pickle
import mimetypes
import os


class Vk:

    @staticmethod
    def make_request(redirect_uri):
        url = f'https://oauth.vk.com/authorize?client_id={settings.VK_APP_CODE}&scope=email&display=popup&redirect_uri={redirect_uri}&response_type=code&v=5.103'
        return url

    @staticmethod
    def login(redirect_uri):
        accept_key_request = requests.get('https://oauth.vk.com/access_token',
                                          params={'client_id': settings.VK_APP_CODE,
                                                  'client_secret': settings.VK_SECRET,
                                                  'redirect_uri': redirect_uri,
                                                  'code': request.args.get('code')})
        session['vk'] = {}
        session['vk']['access_token'] = accept_key_request.json()['access_token']
        session['vk']['user_id'] = accept_key_request.json()['user_id']
        session.modified = True
        user = Users.query.filter_by(vk_id=session['vk']['user_id']).first()
        if user is None:
            first_name, last_name = Vk.get_full_name(access_token=session['vk']['access_token'],
                                                     user_id=session['vk']['user_id'])
            user = Users(vk_id=session['vk']['user_id'], vk_name=first_name, vk_lastname=last_name)
            user.save()
            user.login()
        else:
            user.login()
        Yandex().__init__()
        Google().__init__()

    @staticmethod
    def get_full_name(access_token, user_id):
        vk_request = requests.get(f'https://api.vk.com/method/users.get?user_ids={user_id}&'
                                  f'access_token={access_token}&v=5.103')
        return vk_request.json()['response'][0]['first_name'], vk_request.json()['response'][0]['last_name']


class AbstractClass:

    def __init__(self):
        self.name = self.__class__.__name__.lower()

    @classmethod
    def is_authenticated(cls):
        print(session)
        return True if cls.__name__.lower() in session and session[cls.__name__.lower()].get(
            'is_authenticated') is True else False

    @classmethod
    def drive_logout(cls):
        if cls.__name__.lower() in session:
            del session[cls.__name__.lower()]

    @staticmethod
    def filename(file):
        return file.name[file.name.rfind('\\') + 1:]

    @staticmethod
    def mimetype(file):
        return mimetypes.guess_type(AbstractClass.filename(file))


class Yandex(AbstractClass):

    @staticmethod
    def make_request(redirect_uri):
        url = f'https://oauth.yandex.ru/authorize?response_type=code&client_id={settings.YA_APP_CODE}&redirect_uri=' \
              f'{redirect_uri}&force_confirm=true'
        return url

    @staticmethod
    def get_data():
        if current_user.is_authenticated:
            session['yandex'] = current_user.yandex_drive
            Yandex.get_disk_space_info()
            session.modified = True

    @staticmethod
    def login(code):
        data = {
            'code': code,
            'grant_type': 'authorization_code',
            'client_id': settings.YA_APP_CODE,
            'client_secret': settings.YA_APP_PASSWORD
        }
        accept_key_request = requests.post(
            f'https://oauth.yandex.ru/token',
            headers={'Content-Type': 'application/x-www-form-urlencoded'}, data=data)
        yandex_passport_request = requests.get(
            f'https://login.yandex.ru/info?format=json&oauth_token={accept_key_request.json()["access_token"]}')
        email = yandex_passport_request.json()['default_email']
        if 'yandex' not in session:
            session['yandex'] = {}
            if 'is_authenticated' not in 'yandex':
                session['yandex'].update(is_authenticated=False)
            if 'current_folder' not in 'yandex':
                session['yandex'].update(current_folder='root')
            if 'current_path' not in 'yandex':
                session['yandex'].update(current_path='/')
        session['yandex'].update(access_token=accept_key_request.json()['access_token'],
                                 refresh_token=accept_key_request.json()['access_token'],
                                 email=email,
                                 is_authenticated=True)
        Yandex.get_disk_space_info()
        print(session)
        session.modified = True

    def refresh_token(self):
        data = {
            'grant_type': 'refresh_token',
            'refresh_token': session[self.name]['refresh_token']
        }
        accept_key_request = requests.post(
            f'https://oauth.yandex.ru/token',
            headers={'Content-Type': 'application/x-www-form-urlencoded',
                     'Authorization': 'OAuth ' + session[self.name]['access_token']}, data=data)
        session[self.name]['access_token'] = accept_key_request.json()['access_token']
        session[self.name]['refresh_token'] = accept_key_request.json()['refresh_token']
        session.modified = True

    @staticmethod
    def get_diskinfo(fields):
        get_request = requests.get(f'https://cloud-api.yandex.net/v1/disk?fields={fields}',
                                   headers={'Authorization': 'OAuth ' + session['yandex']['access_token']})
        return get_request.json()

    @staticmethod
    def get_disk_space_info():
        info = Yandex.get_diskinfo('total_space, trash_size, used_space')
        session['yandex'].update(total_space=round(info['total_space'] / 1073741824, 2),
                                 trash_size=round(info['trash_size'] / 1073741824, 2),
                                 used_space=round(info['used_space'] / 1073741824, 2))
        session.modified = True

    @staticmethod
    def get_rootdir():
        return Yandex.get_dir(path='/')

    @staticmethod
    def get_dir(path):
        fields = '_embedded.items.name,' \
                 '_embedded.items.type,' \
                 '_embedded.items.path,' \
                 '_embedded.items.public_key,' \
                 '_embedded.items.public_url'
        get_request = requests.get(
            f'https://cloud-api.yandex.net:443/v1/disk/resources?path={path}&fields={fields}&limit=10000',
            headers={'Authorization': 'OAuth ' + session['yandex']['access_token']})
        if get_request.status_code == 200:
            items = get_request.json()['_embedded']['items']
            for item in items:
                item = Yandex.encode_item(item)
            return items

    @staticmethod
    def get_meta(path, return_fields: str = None):
        if return_fields is not None:
            url = f'https://cloud-api.yandex.net:443/v1/disk/resources?path={path}&fields={return_fields}'
        else:
            url = f'https://cloud-api.yandex.net:443/v1/disk/resources?path={path}'
        get_request = requests.get(url, headers={'Authorization': 'OAuth ' + session['yandex']['access_token']})
        if get_request.status_code == 200:
            return get_request.text
        else:
            app.logger.error(f'get_meta-{get_request.json()["error"]} {get_request.json()["message"]}')

    @staticmethod
    def geturl(path):
        get_request = requests.get(
            f'https://cloud-api.yandex.net/v1/disk/resources/upload?path={path}&fields=href',
            headers={'Authorization': 'OAuth ' + session['yandex']['access_token']})
        if get_request.status_code == 200:
            return get_request.json()['href']
        elif get_request.status_code == 507:
            flash(f'{get_request.json()["message"]}', 'errors')
        else:
            app.logger.error(f'put_geturl-{get_request.json()["error"]} {get_request.json()["message"]}')

    @staticmethod
    def put_file(file, path):
        put_request = requests.put(f'{Yandex.geturl(path)}', data=file,
                                   headers={'Authorization': 'OAuth ' + session['yandex']['access_token']})
        if put_request.status_code == 412:
            app.logger.error(
                'PUT_file - Precondition Failed — при дозагрузке файла был передан неверный диапазон'
                ' в заголовке Content-Range')
        elif put_request.status_code == 413:
            flash('Размер файла превышает 10 ГБ', 'errors')
        elif put_request.status_code == 500 or 503:
            flash('Ошибка сервера, попробуйте повторить загрузку', 'errors')
        elif put_request.status_code == 507:
            flash('Недостаточно места на диске', 'errors')
        Yandex.get_disk_space_info()

    @staticmethod
    def get_download(path):
        get_request = requests.get(f'https://cloud-api.yandex.net/v1/disk/resources/download?path={path}',
                                   headers={'Authorization': 'OAuth ' + session['yandex']['access_token']})
        if get_request.status_code == 200:
            return get_request.text
        elif get_request.status_code == 403:
            flash(f'{get_request.json()["message"]}')
        else:
            app.logger.error(f'get_download-{get_request.json()["error"]} {get_request.json()["message"]}')

    @staticmethod
    def put_mkdir(parent, name):
        if parent == '/':
            path = parent + name
        else:
            path = parent + '/' + name
        put_request = requests.put(f'https://cloud-api.yandex.net/v1/disk/resources?path={path}',
                                   headers={'Authorization': 'OAuth ' + session['yandex']['access_token']})
        if put_request.status_code == 201:
            pass
        elif put_request.status_code in (409, 507):
            flash(f'{put_request.json()["message"]}')
        else:
            app.logger.error(f'put_mkdir-{put_request.json()["error"]} {put_request.json()["message"]}')

    @staticmethod
    def delete(path, permanently):
        delete_request = requests.delete(f'https://cloud-api.yandex.net/v1/disk/resources?path={path}&'
                                         f'permanently={permanently}',
                                         headers={'Authorization': 'OAuth ' + session['yandex']['access_token']})
        if delete_request.status_code in (202, 204):
            Yandex.get_disk_space_info()
        elif delete_request.status_code == 409:
            flash(f'{delete_request.json()["message"]}')
        else:
            app.logger.error(f'delete-{delete_request.json()["error"]} {delete_request.json()["message"]}')

    @staticmethod
    def move(_from, path, overwrite):
        move_request = requests.post(
            f'https://cloud-api.yandex.net/v1/disk/resources/move?from={_from}&path={path}&overwrite={overwrite}',
            headers={'Authorization': 'OAuth ' + session['yandex']['access_token']})
        if move_request.status_code in (403, 409, 507, 503):
            return dict(success=False, message=move_request.json().get("message"))
        else:
            return dict(success=True)

    @staticmethod
    def rename(_from, new_name):
        if eval(Yandex.get_meta(_from, 'type'))['type'] != 'dir':
            file_extension = _from[_from.rfind('.'):]
        else:
            file_extension = ''
        if _from.rfind('/') == -1:
            new_path = new_name + file_extension
        else:
            new_path = _from[:_from.rfind('/')] + '/' + new_name + file_extension
        Yandex.move(_from, new_path, 'false')

    @staticmethod
    def get_root_published_dir():
        get_request = requests.get(f'https://cloud-api.yandex.net/v1/disk/resources/public?limit=100000',
                                   headers={'Authorization': 'OAuth ' + session['yandex']['access_token']})
        if get_request.status_code == 200:
            items = get_request.json()['items']
            for item in items:
                item = Yandex.encode_item(item)
                item['published'] = True
            return items
        if get_request.status_code in range(403, 407):
            app.logger.error(f'get_root_published_dir-{get_request.json()["error"]} {get_request.json()["message"]}')
        if get_request.status_code == (423, 429, 503, 507):
            flash(f'{get_request.json()["message"]}')

    @staticmethod
    def publish(path):
        put_request = requests.put(f'https://cloud-api.yandex.net/v1/disk/resources/publish?path={path}',
                                   headers={'Authorization': 'OAuth ' + session['yandex']['access_token']})
        if put_request.status_code == 200:
            return Yandex.get_meta(path, 'public_url')
        if put_request.status_code in range(403, 407):
            app.logger.error(f'publish-{put_request.json()["error"]} {put_request.json()["message"]}')
        if put_request.status_code == (423, 429, 503, 507):
            flash(f'{put_request.json()["message"]}')

    @staticmethod
    def unpublish(path):
        put_request = requests.put(f'https://cloud-api.yandex.net/v1/disk/resources/unpublish?path={path}',
                                   headers={'Authorization': 'OAuth ' + session['yandex']['access_token']})
        if put_request.status_code == 200:
            return redirect(url_for('main.update_page'))
        if put_request.status_code == 407:
            app.logger.error(f'unpublish-{put_request.json()["error"]} {put_request.json()["message"]}')
        if put_request.status_code == (423, 429, 503, 403):
            flash(f'{put_request.json()["message"]}')

    @staticmethod
    def trash_delete(path):
        """
        Почему-то не поддерживается апи
        """
        delete_request = requests.delete(
            f'https://cloud-api.yandex.net:443/v1/disk/trash/resources?path={path}&force_async=True',
            headers={'Authorization': 'OAuth ' + session['yandex']['access_token']})
        if delete_request.status_code == 409:
            flash(f'{delete_request.json()["message"]}')
        else:
            app.logger.error(f'trash_delete-{delete_request.json()["error"]} {delete_request.json()["message"]}')
        Yandex.get_disk_space_info()

    @staticmethod
    def trash_get():
        fields = '_embedded.items.name,_embedded.items.type,_embedded.items.path'
        path = '/'
        get_request = requests.get(
            f'https://cloud-api.yandex.net:443/v1/disk/trash/resources?path={path}&fields={fields}',
            headers={'Authorization': 'OAuth ' + session['yandex']['access_token']})
        if get_request.status_code == 200:
            items = get_request.json()['_embedded']['items']
            for item in items:
                item = Yandex.encode_item(item)
                item['trash'] = True
            return items
        else:
            app.logger.error(f'trash_get-{get_request.json()["error"]} {get_request.json()["message"]}')

    @staticmethod
    def trash_clean():
        items = Yandex.trash_get()
        for item in items:
            Yandex.trash_delete(item['path'])

    @staticmethod
    def trash_restore(path):
        put_request = requests.put(f'https://cloud-api.yandex.net/v1/disk/trash/resources/restore?path={path}',
                                   headers={'Authorization': 'OAuth ' + session['yandex']['access_token']})
        if put_request.status_code in (403, 507, 503):
            flash(f'{put_request.json()["message"]}')
        else:
            app.logger.error(f'trash_delete-{put_request.json()["error"]} {put_request.json()["message"]}')

    @staticmethod
    def have_primary_folders(item):
        items = Yandex.get_dir(item['path'])
        for elem in items:
            if elem['type'] == 'dir':
                item['have_folders'] = True
                return item
        item['have_folders'] = False
        return item

    @staticmethod
    def render_current_path():
        if Yandex.is_authenticated():
            if session['yandex']['current_folder'] == 'root':
                return Yandex.get_dir(session['yandex']['current_path'])
            elif session['yandex']['current_folder'] == 'trash':
                Yandex.trash_get()
            elif session['yandex']['current_folder'] == 'public':
                Yandex.get_root_published_dir()

    @staticmethod
    def get_current_path():
        if Yandex.is_authenticated():
            if session['yandex']['current_folder'] == 'root':
                return 'root', session['yandex']['current_path']
            elif session['yandex']['current_folder'] == 'trash':
                return 'trash', session['yandex']['current_path']
            elif session['yandex']['current_folder'] == 'public':
                return 'public', session['yandex']['current_path']

    @staticmethod
    def set_current_path(path, folder='root'):
        """
        allow issue with rendering 2 drives, root trash public
        """
        session['yandex']['current_path'] = path
        session['yandex']['current_folder'] = folder
        session.modified = True

    @staticmethod
    def encode_item(item):
        """
        solving problem with routing to 3rd path-param
        """
        if item.get('public_url') is None:
            if item['type'] == 'dir':
                item['image'] = url_for('static', filename='dir.png')
            else:
                item['image'] = url_for('static', filename='file.png')
        else:
            item['public'] = True
            if item['type'] == 'dir':
                item['image'] = url_for('static', filename='dir_public.png')
            else:
                item['image'] = url_for('static', filename='file_public.png')
        item['js_path'] = Yandex.js_path_encode(item['path'])[6:]
        item['path'] = item['path'][6:]
        return item

    @staticmethod
    def js_path_encode(path):
        '''
        remove forbidden chars from path
        '''
        path = path.replace('/', '-')
        path = path.replace('\\', 'bs')
        path = path.replace('.', 'dot')
        path = path.replace('*', 'zv')
        path = path.replace('<', 'ltcv')
        path = path.replace('>', 'rtcv')
        path = path.replace('|', 'vert')
        path = path.replace('(', 'skobl')
        path = path.replace(')', 'skobr')
        path = path.replace('+', 'plus')
        path = path.replace('$', 'dol')
        path = path.replace('#', 'hash')
        path = path.replace('@', 'dog')
        path = path.replace('?', 'vopr')
        path = path.replace(':', 'ddot')
        splitted_path = path.split()
        path = '-'.join(splitted_path)
        return path


class Google(AbstractClass):

    @staticmethod
    def new_make_request():
        flow = google_auth_oauthlib.flow.Flow.from_client_secrets_file(
            'client_secret_775617427333-mkbfn9dnfdm8oelrk40ftmfldt40e7q3.apps.googleusercontent.com.json',
            scopes=settings.GOOGLE_SCOPE)
        flow.redirect_uri = request.url_root + url_for('auth.google_login_response')[1:]
        authorization_url, state = flow.authorization_url(access_type='offline', include_granted_scopes='true',
                                                          prompt='consent')
        session['google'] = dict(state=state)
        session.modified = True
        return authorization_url

    @staticmethod
    def get_data():
        session['google'] = current_user.google_drive
        Google.refresh_token()
        Google.get_disk_space_info()
        session.modified = True

    @staticmethod
    def refresh_token():
        if Google.is_authenticated():
            credentials = pickle.loads(session['google']['credentials'])
            if credentials.expired:
                request = google.auth.transport.requests.Request()
                credentials.refresh(request)
                session['google']['credentials'] = pickle.dumps(credentials)

    @staticmethod
    def credentials_to_dict(credentials):
        return {'token': credentials.token,
                'refresh_token': credentials.refresh_token,
                'token_uri': credentials.token_uri,
                'client_id': credentials.client_id,
                'client_secret': credentials.client_secret,
                'scopes': credentials.scopes}

    @staticmethod
    def new_login():
        flow = google_auth_oauthlib.flow.Flow.from_client_secrets_file(
            'client_secret_775617427333-mkbfn9dnfdm8oelrk40ftmfldt40e7q3.apps.googleusercontent.com.json',
            scopes=" ".join(settings.GOOGLE_SCOPE),
            state=session['google']['state'])
        del session['google']['state']
        flow.redirect_uri = request.url_root + url_for('auth.google_login_response')[1:]
        authorization_response = request.url
        flow.fetch_token(authorization_response=authorization_response)
        credentials = flow.credentials
        id_token_decoded = jwt.decode(credentials.id_token, verify=False)
        email = id_token_decoded['email']
        session['google'].update(credentials=pickle.dumps(credentials),
                                 email=email,
                                 is_authenticated=True)
        Google.get_disk_space_info()
        session.modified = True

    @staticmethod
    def encode_item(item):
        if item['mimeType'] == 'application/vnd.google-apps.folder':
            if item['shared']:
                item['image'] = url_for('static', filename='dir_public.png')
            else:
                item['image'] = url_for('static', filename='dir.png')
        else:
            if item['shared']:
                item['image'] = url_for('static', filename='file_public.png')
            else:
                item['image'] = url_for('static', filename='file.png')
        return item

    @staticmethod
    def get_dir(parent_id, list_of_folders=('root', 'public')):
        Google.refresh_token()
        """
            list of folders - root, public, trash
            return list of dicts that contain files meta-data
        """
        drive_service = build('drive', 'v3', credentials=pickle.loads(session['google']['credentials']))
        next_token = None
        items = []
        while True:
            root_files = drive_service.files().list(q=f'"{parent_id}" in parents', orderBy='folder', spaces='drive',
                                                    pageSize=1000,
                                                    pageToken=next_token,
                                                    fields='nextPageToken, files(id, name, '
                                                           'mimeType, trashed, shared, permissions)').execute()
            for item in root_files.get('files', []):
                if item['trashed'] and 'trash' not in list_of_folders:
                    continue
                if item['shared'] and 'public' not in list_of_folders:
                    continue
                if (not item['trashed'] and not item['shared']) and 'root' not in list_of_folders:
                    continue
                items.append({
                    'name': item['name'],
                    'id': item['id'],
                    'mimeType': item['mimeType'],
                    'shared': item['shared'],
                    'trashed': item['trashed']
                })
            next_token = root_files.get('nextPageToken', None)
            if next_token is None:
                break
        return map(Google.encode_item, items)

    @staticmethod
    def get_root_dir_id():
        Google.refresh_token()
        drive_service = build('drive', 'v3', credentials=pickle.loads(session['google']['credentials']))
        response = drive_service.files().get(fileId='root', fields='id').execute()
        return response['id']

    @staticmethod
    def get_rootdir():
        """
        especially for root dir
        return list of dicts that contain files meta-data
        """
        rootdir_id = Google.get_root_dir_id()
        Google.set_current_path(rootdir_id, 'root')
        return Google.get_dir(rootdir_id)

    @staticmethod
    def trash_get():
        rootdir_id = Google.get_root_dir_id()
        Google.set_current_path(rootdir_id, 'trash')
        return Google.get_dir(rootdir_id, ['trash'])

    @staticmethod
    def public_get():
        rootdir_id = Google.get_root_dir_id()
        Google.set_current_path(rootdir_id, 'public')
        return Google.get_dir(rootdir_id, ['public'])

    @staticmethod
    def put_mkdir(parent_id, name):
        Google.refresh_token()
        drive_service = build('drive', 'v3', credentials=pickle.loads(session['google']['credentials']))
        body = {
            'name': name,
            'mimeType': "application/vnd.google-apps.folder",
            'parents': [{'id': parent_id}]
        }
        drive_service.files().create(body=body).execute()

    def is_file_exist(self, parent_id, name):
        if name in [item['name'] for item in self.get_dir(parent_id)]:
            return True
        else:
            return False

    @staticmethod
    def put_file(file):
        Google.refresh_token()
        drive_service = build('drive', 'v3', credentials=pickle.loads(session['google']['credentials']))
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], file.filename))
        body = {
            'name': file.filename,
            'parents': [Google.get_root_dir_id()]
        }
        file_media = MediaFileUpload(os.path.join(app.config['UPLOAD_FOLDER'], file.filename))
        responce = drive_service.files().create(body=body, media_body=file_media).execute()

        return responce

    @staticmethod
    def get_meta(file_id, fields):
        Google.refresh_token()
        drive_service = build('drive', 'v3', credentials=pickle.loads(session['google']['credentials']))
        file_meta = drive_service.files().get(fileId=file_id, fields=fields).execute()
        return file_meta

    @staticmethod
    def update_file(file_id, data=None, removeparents=None, addparents=None):
        Google.refresh_token()
        drive_service = build('drive', 'v3', credentials=pickle.loads(session['google']['credentials']))
        response = drive_service.files().update(fileId=file_id, body=data,
                                                removeParents=removeparents, addParents=addparents).execute()
        app.logger.info(response)
        return response

    @staticmethod
    def move_file(file_id, new_folder_id):
        response = Google.update_file(file_id,
                                      removeparents=','.join(Google.get_meta(file_id, fields='parents')['parents']),
                                      addparents=new_folder_id)
        return dict(success=True, message=response)

    @staticmethod
    def public_file(file_id):
        Google.update_file(file_id, {'permissions.role': 'reader',
                                     'permissions[(type)]': 'anyone',
                                     'permissions[(id)]': 'anyoneWithLink'})

    @staticmethod
    def unpublic_file(file_id):
        Google.update_file(file_id, {'shared': False})

    @staticmethod
    def rename_file(file_id, new_name: str):
        file_extension = Google.get_meta(file_id, 'fileExtension').get('fileExtension', None)
        if file_extension is not None:
            body = dict(name=new_name + '.' + file_extension)
        else:
            body = dict(name=new_name)
        Google.update_file(file_id, body)

    @staticmethod
    def delete_file(file_id, permanent):
        Google.refresh_token()
        drive_service = build('drive', 'v3', credentials=pickle.loads(session['google']['credentials']))
        if permanent == True:
            response = drive_service.files().delete(fileId=file_id).execute()
            Google.get_disk_space_info()
            return response
        else:
            body = {
                'trashed': True
            }
            response = drive_service.files().update(fileId=file_id, body=body).execute()
            return response

    @staticmethod
    def get_current_path():
        if Google.is_authenticated():
            if session['google']['current_folder'] == 'root':
                return 'root', session['google']['current_path']
        else:
            return None

    @staticmethod
    def set_current_path(_id, folder='root'):
        """
        allow issue with rendering 2 drives, root trash public
        """
        session['google']['current_path'] = _id
        session['google']['current_folder'] = folder
        session.modified = True

    @staticmethod
    def render_current_path():
        if Google.is_authenticated():
            if session['google']['current_folder'] == 'root':
                return Google.get_dir(session['google']['current_path'])
        else:
            return None

    @staticmethod
    def get_diskinfo(fields):
        if not Google.is_authenticated():
            return None
        Google.refresh_token()
        drive_service = build('drive', 'v3', credentials=pickle.loads(session['google']['credentials']))
        response = drive_service.about().get(fields=fields).execute()
        return response

    @staticmethod
    def get_disk_space_info():
        if not Google.is_authenticated():
            return None
        info = Google.get_diskinfo('storageQuota')
        session['google'].update(total_space=round(int(info['storageQuota']['limit']) / 1073741824, 2),
                                 trash_size=round(int(info['storageQuota']['limit']) / 1073741824, 2),
                                 used_space=round(int(info['storageQuota']['usage']) / 1073741824, 2))
        session.modified = True
