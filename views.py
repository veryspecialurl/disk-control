from auth.views import auth
from main.views import main
from manage import app

app.register_blueprint(auth)
app.register_blueprint(main)

