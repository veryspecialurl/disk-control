from flask import Blueprint, render_template, url_for, request, jsonify, redirect, make_response
from main.forms import NewDir, RenameFile, UploadFile, GoogleNewDir, GoogleRenameFile, GoogleUploadFile
from flask_login import current_user
import auth.backends as Drive
import os

main = Blueprint('main', __name__, template_folder='template', static_folder='static')


def standart_context(dir):
    context = {
        'form': NewDir(),
        'RenameForm': RenameFile(),
        'UploadForm': UploadFile(),
        'NewDirGoogle': GoogleNewDir(),
        'GoogleRenameFile': GoogleRenameFile(),
        'GoogleUploadForm': GoogleUploadFile()
    }
    dir.update(context)
    return dir


@main.route('/')
def main_page():
    context = standart_context({})
    print('')
    print('')
    print('')
    print(Drive.Yandex.is_authenticated())
    print(Drive.Google.is_authenticated())
    print('')
    print('')
    print('')
    if Drive.Yandex.is_authenticated() or Drive.Google.is_authenticated():
        if Drive.Yandex.is_authenticated():
            context.update(yandex_items=Drive.Yandex.get_rootdir())
        if Drive.Google.is_authenticated():
            context.update(google_items=Drive.Google.get_rootdir())
        #Drive.Yandex.set_current_path('/')
        # When load google_root_dir, set current path call in get_rootdir() method
        return render_template('main/main_page.html', **context)
    else:
        return render_template('main/main_page_isnt_auth.html', **context)


# --------------------------------------------YANDEX--------------------------------------------------------------------#

@main.route('/y/')
def yandex_root_dir():
    context = standart_context({
        'yandex_items': Drive.Yandex.get_rootdir(),
        'google_items': Drive.Google().render_current_path()
        # Drive.Google().get_current_path()
    })
    Drive.Yandex.set_current_path('/')
    return render_template('main/main_page.html', **context)


@main.route('/<path:path>')
def render_yandex_dir(path):
    context = standart_context({
        'yandex_items': Drive.Yandex.get_dir(path),
        'google_items': Drive.Google().render_current_path()
        # Drive.Google().get_current_path()
    })
    Drive.Yandex.set_current_path(path)
    return render_template('main/main_page.html', **context)


@main.route('/ydownload/<path:path>', methods=['GET'])
def yandex_download(path):
    return jsonify(Drive.Yandex.get_download(path))


# -Trash----------------------------------------------------------------------------------------------------------------#

@main.route('/ydelete/<path:path>+<permanent>')
def yandex_delete(path, permanent):
    Drive.Yandex.delete(path, permanent)
    return '', 204


@main.route('/trash/')
def trash_root_dir():
    context = standart_context({
        'yandex_items': Drive.Yandex.trash_get(),
        'google_items': Drive.Google.render_current_path()
        # Drive.Google().get_current_path()
    })
    Drive.Yandex.set_current_path('trash/', 'trash')
    return render_template('main/main_page.html', **context)


@main.route('/yrestore/<path:path>')
def restore_file(path):
    Drive.Yandex.trash_restore(path)
    return '', 204


@main.route('/ytrash_clear/<path:path>')
def yandex_trash_delete(path):
    Drive.Yandex.trash_delete(path)
    return '', 204


# ----------------------------------------------------------------------------------------------------------------------#

@main.route('/ynew_folder/', methods=['POST'])
def yandex_newfolder():
    form = NewDir(request.form)
    if form.validate():
        catalog, parent = Drive.Yandex.get_current_path()
        name = form.name.data
        if catalog == 'root':
            Drive.Yandex.put_mkdir(parent, name)
            return redirect(url_for('main.update_page'))
    else:
        return '', 204


@main.route('/public/')
def public_root_dir():
    context = standart_context({
        'yandex_items': Drive.Yandex.get_root_published_dir(),
        'google_items': Drive.Google.render_current_path()
        # Drive.Google().get_current_path()
    })
    Drive.Yandex.set_current_path('public/', 'public')
    return render_template('main/main_page.html', **context)


@main.route('/yrename/', methods=['POST'])
def rename_file():
    form = RenameFile(request.form)
    if form.validate():
        old_name = form.preveousname.data
        new_name = form.name.data
        Drive.Yandex.rename(old_name, new_name)
    return redirect(url_for('main.update_page'))


@main.route('/ypublish/<path:path>', methods=['GET'])
def yandex_publish(path):
    return jsonify(Drive.Yandex.publish(path))


@main.route('/yunpublish/<path:path>')
def yandex_unpublish(path):
    return Drive.Yandex.unpublish(path)


@main.route('/upd/')
def update_page():
    context = standart_context({
        'yandex_items': Drive.Yandex.render_current_path(),
        'google_items': Drive.Google.render_current_path()
    })
    return render_template('main/main_page.html', **context)


@main.route('/yupload/', methods=['POST'])
def yandex_upload():
    form = UploadFile(request.form)
    file = request.files['file']
    path = form.path.data.strip()
    if path == '/':
        path = ''
    path = path + '/' + file.filename
    Drive.Yandex.put_file(file, path)
    return redirect(url_for('main.update_page'))

@main.route('/yloadfolderss/')
def yandex_load_root():
    items = [item for item in Drive.Yandex.get_dir('/') if item['type'] == 'dir']
    return jsonify(items=items)

@main.route('/yloadfolders/<path:path>')
def yandex_load_folders(path):
    items = [item for item in Drive.Yandex.get_dir(path) if item['type'] == 'dir']
    return jsonify(items=items)

@main.route('/yandexmovefile/')
def yandex_move_file():
    path_from = request.args.get('path_from')
    path_to = request.args.get('path_to', None)
    if path_to is not None:
        path_to = path_to + '/' + request.args.get('name')
    else:
        path_to = request.args.get('name')
    dict = Drive.Yandex.move(path_from, path_to, True)
    return jsonify(dict)
# --------------------------------------------YANDEX--------------------------------------------------------------------#


# --------------------------------------------GOOGLE--------------------------------------------------------------------#

@main.route('/g/')
def google_root_dir():
    context = standart_context({
        'yandex_items': Drive.Yandex.render_current_path(),
        'google_items': Drive.Google.get_rootdir()
    })
    Drive.Yandex.set_current_path('/')
    return render_template('main/main_page.html', **context)


@main.route('/g/<path:_id>')
def render_google_dir(_id):
    context = standart_context({
        'yandex_items': Drive.Yandex.render_current_path(),
        'google_items': Drive.Google.get_dir(_id)
    })
    Drive.Google.set_current_path(_id)
    return render_template('main/main_page.html', **context)


@main.route('/gnew_folder/', methods=['POST'])
def google_newfolder():
    form = GoogleNewDir(request.form)
    if form.validate():
        catalog, parent = Drive.Google.get_current_path()
        name = form.name.data
        if catalog == 'root':
            Drive.Google.put_mkdir(parent, name)
            return redirect(url_for('main.update_page'))
    else:
        return '', 204


@main.route('/gdelete/<path:_id>+<permanent>')
def google_delete(_id, permanent):
    if permanent == 'true':
        Drive.Google.delete_file(_id, True)
    else:
        Drive.Google.delete_file(_id, False)
    return '', 204

@main.route('/gtrash/')
def gtrash_root_dir():
    context = standart_context({
        'yandex_items': Drive.Yandex.render_current_path(),
        'google_items': Drive.Google.trash_get()
    })
    # google set current path in trash get
    return render_template('main/main_page.html', **context)

@main.route('/gpublic/')
def gpublic_root_dir():
    context = standart_context({
        'yandex_items': Drive.Yandex.render_current_path(),
        'google_items': Drive.Google.public_get()
    })
    # google set current path in trash get
    return render_template('main/main_page.html', **context)

@main.route('/grename/', methods=['POST'])
def google_rename_file():
    form = GoogleRenameFile(request.form)
    if form.validate():
        _id = form.id.data
        new_name = form.name.data
        Drive.Google.rename_file(_id, new_name)
        return redirect(url_for('main.update_page'))
    else:
        return '', 204

@main.route('/ggetlink/<itemid>')
def google_download(itemid):
    link = Drive.Google.get_meta(itemid, 'webContentLink').get('webContentLink', None)
    if link is not None:
        return jsonify(href=link)
    else:
        return jsonify(href='Временно функция скачивания папок и некоторых типов файлов недоступна')

@main.route('/gpublish/<itemid>')
def google_publish(itemid):
    Drive.Google.public_file(itemid)
    return '', 204

@main.route('/gunpublish/<itemid>')
def google_unpublish(itemid):
    Drive.Google.unpublic_file(itemid)
    return '', 204

@main.route('/gloadfolderss/')
def google_load_root():
    items = [item for item in Drive.Google.get_rootdir() if item['mimeType'] == 'application/vnd.google-apps.folder']
    return jsonify(items=items)

@main.route('/gloadfolders/<path:_id>')
def google_load_folders(_id):
    items = [item for item in Drive.Google.get_dir(_id) if item['mimeType'] == 'application/vnd.google-apps.folder']
    return jsonify(items=items)

@main.route('/googlemovefile/')
def google_move_file():
    path_from = request.args.get('path_from')
    path_to = request.args.get('path_to', None)
    if path_to is None:
        path_to = Drive.Google.get_root_dir_id()
    dict = Drive.Google.move_file(path_from, path_to)
    return jsonify(dict)

@main.route('/gupload/', methods=['POST'])
def google_upload():
    Drive.Google.put_file(request.files['file'])
    return redirect(url_for('main.update_page'))

# --------------------------------------------GOOGLE--------------------------------------------------------------------#

@main.route('/passport')
def user_page():
    return render_template('main/user_page.html')
