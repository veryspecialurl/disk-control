from wtforms import Form, StringField, FileField
from wtforms.validators import InputRequired, ValidationError


class NewDir(Form):
    name = StringField('Введите название новой папки', [InputRequired()])

    def validate_name(form, field):
        forbidden_chars = ('/', ':')
        for char in field.data:
            if char in forbidden_chars:
                raise ValidationError(
                    'Название папки не может содержать: ' + ' ; '.join(forbidden_chars))


class RenameFile(Form):
    name = StringField('Введите новое имя фаила...', [InputRequired()])
    preveousname = StringField('', [InputRequired()])

    def validate_name(form, field):
        forbidden_chars = ('/', ':')
        for char in field.data:
            if char in forbidden_chars:
                raise ValidationError(
                    'Название папки не может содержать: ' + ' ; '.join(forbidden_chars))


class UploadFile(Form):
    file = FileField('Выберите файл для загрузки...', [InputRequired()])
    path = StringField('', [InputRequired()])


class GoogleNewDir(Form):
    name = StringField('Введите название новой папки', [InputRequired()])

    def validate_name(form, field):
        forbidden_chars = ('/', ':')
        for char in field.data:
            if char in forbidden_chars:
                raise ValidationError(
                    'Название папки не может содержать: ' + ' ; '.join(forbidden_chars))


class GoogleRenameFile(Form):
    name = StringField('Введите новое имя фаила...', [InputRequired()])
    id = StringField('', [InputRequired()])

    def validate_name(form, field):
        forbidden_chars = ('/', ':')
        for char in field.data:
            if char in forbidden_chars:
                raise ValidationError(
                    'Название папки не может содержать: ' + ' ; '.join(forbidden_chars))

class GoogleUploadFile(Form):
    file = FileField('Выберите файл для загрузки...', [InputRequired()])